﻿using CasaDoCodigo.Models;
using System.Collections.Generic;
using System.Linq;

namespace CasaDoCodigo
{
    public class DataService : IDataService
    {
        private readonly Context _context;

        public DataService(Context context)
        {
            this._context = context;
        }

        public List<ItemPedido> GetItensPedido()
        {
            return _context.ItensPedido.ToList();
        }

        public List<Produto> GetProdutos()
        {
            return _context.Produtos.ToList();
        }

        public void InicializaDb()
        {
            _context.Database.EnsureCreated();
            if (_context.Produtos.Count() == 0)
            {
                List<Produto> produtos = new List<Produto> {
                    new Produto("Sleep Not Found", 59.90m),
                    new Produto("May the Code Be With You", 59.90m),
                    new Produto("RollBack", 59.90m),
                    new Produto("REST", 69.90m),
                    new Produto("Designer Patter Com Java", 69.90m),
                    new Produto("Vire o Jogo com Spring Framework", 69.90m),
                    new Produto("TDD", 69.90m),
                    new Produto("iOs", 69.90m),
                    new Produto("Desenvolvimento de Jogos com Android", 69.90m)
                };
                foreach (var produto in produtos)
                {
                    _context.Produtos.Add(produto);
                    _context.ItensPedido.Add(new ItemPedido(produto, 1));
                }

                _context.SaveChanges();
            }
        }
    }
}
