﻿namespace CasaDoCodigo.Models
{
    public class ItemPedido
    {
        public ItemPedido()
        {

        }
        public ItemPedido(int id, Produto produto, int quantidade) : this(produto, quantidade)
        {
            Id = id;
        }

        public ItemPedido(Produto produto, int quantidade)
        {
            Produto = produto;
            Quantidade = quantidade;
            PrecoUnitario = produto.Preco;
        }
        public int Id { get; private set; }
        public Produto Produto { get; private set; }
        public int Quantidade { get; private set; }
        public decimal PrecoUnitario { get; private set; }
        public decimal Subtotal { get { return Quantidade * PrecoUnitario; } }

    }
}
