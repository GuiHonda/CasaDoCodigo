﻿namespace CasaDoCodigo.Models
{
    public class Produto
    {
        public Produto()
        {

        }
        public Produto(int id, string nome, decimal preco) : this(nome, preco)
        {
            Id = id;
        }

        public Produto(string nome, decimal preco)
        {
            Nome = nome;
            Preco = preco;
        }
        public int Id { get; private set; }
        public string Nome { get; private set; }
        public decimal Preco { get; private set; }
    }
}
