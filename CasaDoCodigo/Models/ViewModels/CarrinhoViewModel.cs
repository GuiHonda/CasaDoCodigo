﻿using System.Collections.Generic;
using System.Linq;

namespace CasaDoCodigo.Models.ViewModels
{
    public class CarrinhoViewModel
    {
        public List<ItemPedido> Itens { get; private set; }
        public decimal Total
        {
            get
            {
                return Itens.Sum(c => c.Subtotal);
            }
        }
        public CarrinhoViewModel(List<ItemPedido> itens)
        {
            Itens = itens;
        }
    }
}
